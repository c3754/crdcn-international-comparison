
# Comparative analysis of the CRDCN as a National Research Data Platform

The purpose of this document is to assess how the CRDCN is structured, operates and performs relative to other national/international organizations that have a comparable function as research platforms providing access to confidential and secure microdata. Further to input from the CRDCN Research Advisory Committee, Joint Operations Committee and Board, focus for this document is on the organizations with substantial similarities to CRDCN. Two main components are envisaged for this document: (A) research data organizations that serve as relevant and instructive comparators; and (B) criteria and metrics for assessing CRDCN’s structure, operations, and competitiveness.

## Section 1 - Research Data Ecosystem Organizations
This document focuses on access to confidential microdata in other countries. A short description of the organizations is provided below.

1. Canadian Research Data Centre Network (CRDCN)

> The CRDCN is a network of universities in Canada with an on-site facility to access Statistics Canada microdata as well as program data from government organizations both federal and provincial. These facilities vary in size, but provide access to both data and computing resources on-site to allow researchers to conduct research. The CRDCN's mandate is to be "Canada’s national platform for leading edge research and training in the quantitative social and population health sciences, creating social, economic, health and environmental benefits for Canadians". 

2. Federal Statistical Research Data Centres (FSRDC)

> Federal Statistical Research Data Centers (RDCs) are Census Bureau facilities, housed in partner institutions, that meet all physical and information security requirements for access to restricted-use micro data of the agencies whose data are accessed there.

> RDC researchers have access to computing capacity to handle large datasets and complex calculations. Standard statistical, econometric, and programming software, including Stata, SAS®, R, MATLAB and Anaconda python are available in a Linux environment. RDC researchers can collaborate with other RDC researchers across the U.S. through the secure RDC computing environment.

3. Centre d'accès Sécurisé aux Données (CASD)

> CASD is the French remote access facility providing access for research to a vast catalogue of confidential data from Insee (the national statistical institute) and the Statistical Services in the Ministries (SSMs), from the Central Bank as well as from various administrations, government agencies or research institutions. Initially established by Insee (French National Statistical Institute) in 2009, CASD is now a Groupe d’Intérêt Public (Public Interest Group) with a legal status, created by decree of 29 December 2018, bringing together INSEE CNRS (Centre National de la Recherche Scientifique) and several universities, GENES (Groupe des Écoles Nationales d’Économie et Statistique), , École Polytechnique and HEC Paris (Hautes Études Commerciales),. The main purpose of the consortium is to organize and implement secure access services for confidential data for non-profit research, study, evaluation or innovation, mainly public. CASD’s technical architecture is designed to enable the processing of detailed confidential data in a secure environment through remote access, guaranteeing the highest possible level of security while enabling users to benefit from a familiar and complete workspace. 

> All processing is done within the centralized secure IT infrastructure, housed in CASD premises. To access this environment from outside, CASD has designed an autonomous thin client named the “SD-Box” with biometric authentication, simple to install and use with low dependency of the local IT. It may be used by more than one authorized user. Each project has its own server, a secure bubble, within the infrastructure allowing the project members to access, see and process the data. The entire chain of installation and start-up of clusters as well as the maintenance of their software resources is carried out by the CASD-IA team. CASD’s “AI and big data” on-demand services aim to accelerate research, innovation and production in the field of Artificial Intelligence and massive data. CASD acts as a third party between the data holders and the data users once they are authorized by the dedicated authorities and/or the data holders. Outputs are checked according to the rules provided by the data holders. 

> It can also act as a third party for data matching. Its mission is also to promote the technology developed to secure remote access to confidential data (both public and private). CASD currently holds 496 data sources (including many products), thus supporting, without silos, analysis requiring sensitive data from many domains including health data (epidemiological cohorts and medico-administrative data). Remote access is provided across borders with the same conditions to researchers based in universities within the European Union or the United Kingdom and with some specific conditions to North America. The CASD is also a member of IDAN (International Data Access Network), a network of 6 Research Data Centres from 4 countries (France, Germany, Netherlands and the UK which aims at facilitating research use of confidential data between these countries. As a first step, researchers can work remotely on data provided by partners from within their local Research Data Centre, all partners’access points being in all partners premises.

4. Forschungdatenzentren (FDZ)

>The basic objective of the Research Data Centres of the Statistical Offices of the Federation and the Federal States is to not only enable the access to official statistics microdata but to continuously improve it and to adapt it to the changing needs of scientists. Information about the program presented in the tables throughout the document refers to the RDC program, not GESIS. 

> The foundation of the RDC stems from a report of the “Commission on the Improvement of the Informational Infrastructure between Research and Statistics” (KVI). Commissioned by the Federal Ministry of Education and Research (BMBF), it has developed various proposals for improving the interrelations between researchers and statistics in 1999. One of the central recommendations of this commission was to set up Research Data Centres on the premises of the public data producers.

> Based on this recommendation and funded by the Federal Ministry of Education and Research in the start-up phase, the Statistical Offices started the establishment of Research Data Centres. In autumn 2001, the Research Data Centre of the Federal Statistical Office was established. In April 2002, the Research Data Centre of the Statistical Offices of the Federal States followed as joint pilot project.

> Like other countries, Germany has public use files, and microdata to be accessed in a secure facility, but also has scientific use files which can be downloaded by approved users (slightly more detailed than ordinary public-use files), and remote code submission (where user-written code is run against masterfiles by personnel at the RDC). For the purposes of this document we exclude scientific use files from access and use figures as they are not the most useful basis for comparison with CRDCN. The FDZ still requires an application for use of the scientific use file, but it is subsequently downloaded by the researcher and there are no costs associated with vetting output (the file is deemed safe enough). 19% of FDZ activity makes use of scientific use files, this activity is concentrated among master's students.

5. Australian Bureau of Statistics Datalab (ABS)

> DataLab is the ABS’s analysis solution for high-end users who want to undertake real time complex analysis of detailed and integrated microdata. It is a complimentary microdata solution; other Microdata products include TableBuilder and MicrodataDownload. The ABS also have a range of aggregated statistics published on their website.
> DataLab allows researchers to view and analyse unit record information in a safe and secure virtual environment but all analytical output are checked by the ABS before release. DataLab is available to researchers who meet ABS safe people criteria. Researchers must belong to a government, university or public policy organisation. Researchers must belong to an Australian organsiation and access the system only from Australia.

6. DataFirst (DF)

> DataFirst is a research data service dedicated to giving open access to African data. The service promotes high quality research by providing the essential Open Research Data infrastructure for discovering and accessing data and by developing skills among prospective users, particularly in South Africa. DataFirst undertakes research on the quality and usability of national data and encourages data usage and data sharing.

> The data service includes an online research data repository, and a Secure Research Data Centre in which researchers can analyse sensitive restricted access data. These data include person- household - and institution-level variables that could make identification of respondents possible. 

> The datasets in the secure centre come from surveys conducted by and on behalf of government agencies. The most popular data product is the *National Income Dynamics Study (NIDS), a South African household-level panel survey*

7. Israel Social Sciences Data Centre (ISDC)

> Originally in scope as an institution in this analysis because of its similarities to the CRDCN program as an institution that was initiated by university researchers and headquartered at a university in partnership with a central statistical agency and agreements with partner universities. The ISDC does not appear in the tables below as it was shut down in 2019. The institute still has an online presence and self-describes as follows:

> Established by the Faculty of Social Sciences at the Hebrew University of Jerusalem, with a mission to collect, preserve and distribute data of interest to the academic community, the ISDC is a major infrastructure for quantitative research of Israeli social and economic issues. Since the early-nineties, it has become a national resource center, disseminating data throughout Israel and, by arrangement with foreign archives, internationally. Its user community includes Israeli universities, colleges and research institutes as well as policy makers in the public and private sectors. The advisory committee, consisting of the member institutions and an observer from the Central Bureau of Statistics, supervises the ISDC activities.
 
> The ISDC now houses approximately 1000 datasets including national sample surveys, local studies, census micro-data and government records in selected fields as well as macro-economic and regional series. Several databases are available through the Web. 

> Sources of data include the Central Bureau of Statistics (CBS), the National Insurance Institute of Israel (NII), central and local government agencies, research institutes as well as independent researchers from affiliated institutes. Since 1995, the ISDC serves also as an authorized distributor of micro-data from Israel Central Bureau of Statistics. Major sources of foreign data are ISSP and ICPSR Archive in Michigan, where ISDC serves as the Official Representative of Israel. 

> The system was one of high-trust where accredited researchers were provided with copies of the microdata through a temporary FTP link or on a CD. Researchers were given guidance on using and publishing statistical information taken from the data and entrusted to use the data for the purposes outlined in their research proposal. The center closed in 2019 due to lack of funding. From the outside, the system did not seem to provide a lot of *added value* as managing the FTP server and performing statistical analyses for hire are functions that could (and seem to have been) easily absorbed by the central statistical agency. The brevity of the communication with the former university lead did not permit inquiry into whether the new arrangement is working more efficiently for both university researchers and the statistical agency, but it is not difficult to think that this represents an overall improvement in access to the *Israeli* data. What academics have most likely lost is institutional access to a local ICPSR member.


## Section 2 - Infrastructure Comparisons

The metrics presented in this section will enable CRDCN to report on its standing relative to international comparator organizations that are positioned within their own national context in the research data ecosystem in a manner similar to CRDCN in Canada. Metrics included in this section will focus primarily on those over which CRDCN has some jurisdiction for decision-making and tracking.

*Table 2.1 - Physical vs. Virtual data access*

|Comparator|CRDCN|FSRDC|CASD|FDZ|ABS|DF|
|---|---|---|---|---|---|---|
|1. Total number of contracts and Researchers|2514 Users, 4972 Projects| 1161 Users, 572 Projects| 1202 Users, 1103 Projects|760 projects | 1772 Users, 503 Projects | 13 Users, 11 Projects|
|2. Share of contracts using a dataset that is available remotely| 0 | 0.13 | 1.0 |1.0* | 1.0 | 0 |
|3. Share of users doing remote work | 0 | 0.10* | 1.0 |0.42| 1.0 | 0 |
|4. Share of new datafiles available virtually| 0 | - | 1.0 |1.0* | 1.0 | 0 |

Notes:

- CRDCN does not currently have a mechanism for remote access. Files above Confidentiality Classification 8 will be accessible only in-person once remote access becomes available. Currently 70% of contracts would be eligible for remote access.

- All new researchers and projects must start their project within the FSRDC facility and may then apply for remote access if sponsoring agency agreements allow. Participation in remote access pilot programs is subject to authorization by the appropriate government agencies, data sponsors, and existing data agreements. Some agencies may approve some of their data for remote access, but not all. Researchers on some projects will not be eligible for remote access because of restrictions associated with a subset of the datasets to which they have access. Census represents the majority of data use in the centres (66%). The data for column three is the share of *projects* doing remote work, not users.

- For CASD, remote access is the only way to access all confidential data via a thin client, the SD-Box, installed in their office in their universities. Remote access is available across borders from the EU countries and associated countries ( + UK) with the same conditions, and with specific conditions from North America

- *In the FDZ, remote access is done on the microdata via remote code submission, rather than remote access to the microdata directly (as we generally think of "remote" access, and is implied by the *virtual data access* heading to the table). The German system has a remote access program in development. In contrast to other countries, remote job submission means that researchers from outside Germany are able to access and work on German microdata. 

- ABS does not have physical sites. Remote access is restricted to "approved workspaces" which are generally university offices or home offices - the door must be lockable and cannot be a communal space.

- DataFirst has no virtual access and no mechanism for virtual access to confidential files. They provide an open data repository service for non-confidential data, but data in the centre can identify individuals (contains lat./long. information about collection site) and is more confidential than data contained in similar services in other countries.

*Table 2.2 - Access to data by location/Stakeholders served*

|Comparator|CRDCN|FSRDC|CASD|FDZ|ABS|DF|
|---|---|---|---|---|---|---|
| 1. Share of users from academia |0.8 | 0.9 |0.7 |1.0 |0.65 | 1.0 | 
| 2. Share of users from government |0.05 | 0.1 |0.15 |0.0 | 0.30| 0 |
| 3. Share of users from private sector |0.18 | <0.01 |0.15 |0.0 | 0.05 | 0 | 

Notes:

- Totals may differ from 1 due to rounding/users of unknown status. 

- Sometimes FSRDC student researchers graduate from academia to private or public sector positions, but these changes to status are not captured by the shares listed.

- In Germany, anyone not conducting independent academic research through an approved institution (university or think-tank) is ineligible to use the services. Thus 100% of users are academic. These users must still apply for access to specific microdata files once their institution is on the list of approved organizations.

- ABS does not allow undergraduate students to access the system. There is also a status of 'discussant' that exists aside from 'analyst' that allows someone to view the confidential aspects of the project, but not have a login. This reduces workloads on analysts as they don't have to process disclosure requests or review non-numeric outputs (unlabeled graphs etc.) for things that are intermediate results.

- DataFirst serves academics almost exclusively with a few researchers from government using the data. There is no restriction on private sector users working with the data, but the private sector does not have the skills to work with the microdata and so they aren't represented among the users. With all access happening at University of Cape Town, there is no access outside of a major centre.


*Table 2.3 - Data availability*

|Comparator|CRDCN|FSRDC|CASD|FDZ|ABS|DF|
|---|---|---|---|---|---|---|
|1. Files available| 212 | 370 |496| 83|74| 34 | 
|2. Files added in the past year | 38 | - | 80| | 6 | 0 |
|3. Updates made to existing files | 31 | 341 | 42| | | 0 |
|4. Number of administrative-survey linked files | 21 | | | | | 0 |

Notes:

- To the maximum extent possible, files are counted consistently across organisations one title = one file even if multiple waves of a survey exist.

- The FSRDC files have update dates in [their system](https://www.researchdatagov.org/search), but dataset updates are not reflected in most of the update date information, thus it isn't possible to determine when the *Data* were updated.

- Most CASD data are multi-year/multi-wave files, not easily disaggregated for consistent counting; 496 is an overstatement of the number of files relative to the other organisations.

- Each [file in the FDZ](https://www.forschungsdatenzentrum.de/de/alle-daten) might have multiple versions: one for scientific use that is less sensitive, and another for secure centre access only. A third in-between version may be created to comply with legislation around remote access once the system is introduced.

- ABS total was taken from [the Datalab Products Page](https://www.abs.gov.au/statistics/microdata-tablebuilder/datalab/products).

- DataFirst data consist of survey files and administrative records. The most recent update to any file was in 2020.


## Section 3 - Governance, Funding and Expenditure Summary

### Governance Model

##### 1. CRDCN:
	
The CRDCN exists as a partnership between over 30 universities in Canada and Statistics Canada. The organization is headquartered at McMaster University with a variety of governing bodies including a Board of Directors, an Academic Council made up of professors representing the institutions which host a data lab, and a small central staff that manages the funds and provide administrative support to the program including some centralized training offerings.

##### 2. FSRDC:

The FSRDC is a program run by the Census Bureau's Center for Enterprise Dissemination through Universities and Federal Reserve banks across the US which play host to RDCs. Funding is provided by the data providers and the RDC sites (who provide cash funding as well as in-kind funding in the form of lab space.) Each site has a lot of control over their own budgeting and funding model which leads to large variations in how projects are charged/who gets charged.

##### 3. CASD:

CASD, as a group whose mission is to implement secure access services, is overseen by representatives of the National statististical Institute/ Insee together with CNRS, Centre national de la recherche scientifique, and several major French universities, GENES (Groupe des écoles nationales d’économie et statistique), HEC Paris and École Polytechnique. There are three committees upon which the director can draw when making decisions: The Scientific Council, the Data Holders Committee, and the Information System Security Policy Monitoring Committee. These groups assist the General Assembly and the Director in matters of strategy, innovation and ethics; access conditions, documentation and archiving; and system security governance respectively. CASD is not in charge of the users’ approval which depend on dedicated independent authorities: for most data approval is based on an advice from the Comité du Secret Statistique, an independent authority chaired by a Judge with representatives of the Parliament, of the Unions, of the researchers and the data holders/data producers, with decision from the National Archives. In any case, the decision requires the approval of the data producer/data holder.
Funding mainly comes from the fees paid by the users (about 65%) and a small part from the consortium partners (25-30%); the remainder varies year-by-year, one example is revenues from the patent on the SD-Box)
9-year funding agreement from EQUIPEX paid a comparable role to CFI in the Canadian context by establishing the infrastructure.

##### 4. FDZ:

The FDZ system is run by the central statistical agency. At least one secure centre is available in each county, with multiple centres available in large cities or counties with several large cities. Costs are partly covered by the government, but user-fees are meant to make up the remainder of the budget. Similar to CASD, each project pays a charge that is specific to their status (student vs. non-student), and data needs. Providing remote job execution is the most costly means of providing access, since the agency vets every piece of output for disclosure, even if the researcher might consider it to be an intermediate output. FDZ is re-evaluating their pricing for modes of access. Remote execution will soon be more costly than secure centre access which will be more costly than scientific use data.

##### 5. ABS:

Datalab is maintained by the Statistical Production and Digital Services (SPDS) Division of the Australian Bureau of Statistics and nis partially funded by the ABS with some government funding and partly funded by users. Universities Australia as a group negotiate a price for access to the Datalab service.

##### 6. DF:

DataFirst is based at the University of Cape Town; the unit’s part-time director is a professor of Economics. DataFirst’s governing bodies include an internal advisory committee made up of Research Deputy Vice-Chancellors, and representatives from commerce and humanities; and a Board of Directors with representatives from ICPSR, UK Data Service, Statistics South Africa, and representatives from the research-intensive universities across the country.


### Expenditures and costs

##### 1. CRDCN:

|Budget item| Description|
|:---|---|
|Cost of a project| A project costs approximately (CAD)6500 for external users (i.e. those not employed by and working for a university in the consortium). There is no recurring cost, but the initial payment covers 200 hours of access. Additional access is purchased in increments of 100 hours at a rate of (CAD)1750.
|Infrastructure support model | The CRDCN is funded by grants from national level government bodies which fund research (SSHRC, CIHR) and research infrastructure (CFI), as well as by provincial governments and university partners. Data are provided by Statistics Canada from their data holdings, they also provide in-kind staff support to CRDCN via the RDC program.|
|Distribution and employment of data custodians | Data custodians are employed by Statistics Canada in the subject matter divisions. In addition to these individuals, the access division which oversees the data in the RDC program have 'persons most responsible' for the data that serve as liaisons between the subject matter division and researchers using the data. |
|Human resources and allocation| CRDCN operates centrally with 13 staff members (An executive director, 3 FTE in research, 3 in communications, 3 in IT, and 2 in Operations/finance). Combining these with the administrative support provided in-kind by Statistics Canada and the partner institutions the total FTE for the network is 85 people. | 

##### 2. FSRDC:

|Budget item| Description|
|:---|---|
|Cost of a project|[Fees are charged](https://www.census.gov/about/adrm/fsrdc/about/fsrdc-network-fees.html) by some data providers to offset the cost of access. Example case is Bureau of Economic Analysis which charges (USD)5,400 for setup and (USD)1,900 annually for the life of the project. Example lab fee is 13,500 (USD) for a five-year project**|
|Infrastructure support model |Federal and state statistical agencies collaborate with the Census Bureau to provide microdata to approved researchers in the secure FSRDC environment. The agencies listed [on this page](https://www.researchdatagov.org/about) contribute data directly to the FSRDCs.|
|Distribution and employment of data custodians | Custodians are employed by the organizations that share their data.|
|Human resources and allocation| Lab directors are employed by the census bureau, each site has its own local director who belongs to the institution hosting the site. There is also a central staff complement at Census Bureau. |

**Project cost example taken from the RDC at Georgetown University (which posts pricing info online).** 

##### 3. CASD

|Budget item| Description|
|:---|---|
|Cost of a project| Each project cost is [calculated](https://www.casd.eu/estimation-de-cout/) based on resources and data required (between 2500 and 3500 Euros). All charges are renewed (projects are typically 4 years - 12 months initial contract then 6 month renewals), charges for students are approximately 60% of what others pay. Charges higher for for-profit sector.|
|Infrastructure support model | Software charges are added to project cost (e.g. MS office - €384/yr), 5 system configurations available (cores+ram) - all custom images (i.e. a menu of off-standard software available including more costly and esoteric offerings like Matlab or SPSS) |
|Distribution and employment of data custodians | Custodians are employed by CASD|
|Human resources and allocation| 27 employees (2 provided in kind by INSEE & CNRS), 3 departments (project management service - 7; IT - 12; data management - 6; executive 2) there is more support staff in-kind from partners (but not on a full time basis)|


##### 4. FDZ

|Budget item| Description|
|:---|---|
|Cost of a project| €250/survey-year. The [total charge](https://www.forschungsdatenzentrum.de/en/user-charge) depends on the number of data files requested and number of modes of access. Students pay a reduced rate (€50). Projects last 3 years, but can be extended. |
|Infrastructure support model | The FDZ program is administrated by the statistical agency via a stand-alone division. Project-fees cover a portion of the total cost of servicing a contract.|
|Distribution and employment of data custodians | The agencies that collect the data provide it to the central statistical office who takes over the data custodianship. Once the data are transferred, there isn't any further contact about which projects are being done or any review of researcher proposals. |
|Human resources and allocation| There is a central staff consisting of 9 people, most of whom are former academics, three of whom are contract employees. Each secure centre has a staff-person   |

- Typical project cost is €8000. The average project has 3 researchers on it.

##### 5. ABS:

|Budget item| Description|
|:---|---|
|Cost of a project|(AUD) 2000-2200 annually|
|Infrastructure support model | Created and maintained by ABS, funded by Treasury Board. Project costs for university researchers are paid by Universities Australia as a lump-sum to the ABS as a payment for service. ABS does not charge external agencies to bring data into the system (though any linkages created are charged at cost-recovery). These agencies can also access the data they've provided to the service without the charge. |
|Distribution and employment of data custodians | ABS is responsible for care and security of external data from different departments. Custodians remain in the department sending the data and have to review all requests for researcher access. In some cases, within specific topics, the ABS can approve access to data sets from other departments.|
|Human resources and allocation| Within the ABS, 110 individuals are responsible for the programming (this includes data integration activities and curation.) |

- Costs do not scale down when adding extra researchers (i.e. 10 researchers is double the cost of 5 etc..) 

- ABS is currently reviewing the costs of providing access to data and is anticipating that fees will increase in the near future. In concert with this update to the fees, the agreement with the universities will be revisited.

##### 6. DataFirst:

|Budget item| Description|
|:---|---|
|Cost of a project| There is no charge to access either the online data or confidential data provided in DataFirst's secure centre.|
|Infrastructure support model | Data First is not supported by government funds. The University of Cape Town supports the project from internal funds, but the bulk of revenue comes from offering training in data management and data analysis. DataFirst is able to charge for this training because they are exclusively positioned to offer it. Training is provided not just to agencies in South Africa, but across the continent. |
|Distribution and employment of data custodians | The staff at Data First are able to conduct proposal reviews and grant access. |
|Human resources and allocation | Data First has four staff members: A manager, one full-time and two part-time data analysts. |




=======
# Comparative analysis of the CRDCN as a National Research Data Platform

The purpose of this document is to assess how the CRDCN is structured, operates and performs relative to other national/international organizations that have a comparable function as research platforms providing access to confidential and secure microdata. Further to input from the CRDCN Research Advisory Committee, Joint Operations Committee and Board, focus for this document is on the organizations with substantial similarities to CRDCN. Two main components are envisaged for this document: (A) research data organizations that serve as relevant and instructive comparators; and (B) criteria and metrics for assessing CRDCN’s structure, operations, and competitiveness.

## Section 1 - Research Data Ecosystem Organizations
This document focuses on access to confidential microdata in other countries. A short description of the organizations is provided below.

1. Canadian Research Data Centre Network (CRDCN)

> The CRDCN is a network of universities in Canada with an on-site facility to access Statistics Canada microdata as well as program data from government organizations both federal and provincial. These facilities vary in size, but provide access to both data and computing resources on-site to allow researchers to conduct research. The CRDCN's mandate is to be "Canada’s national platform for leading edge research and training in the quantitative social and population health sciences, creating social, economic, health and environmental benefits for Canadians". 

2. Federal Statistical Research Data Centres (FSRDC)

> Federal Statistical Research Data Centers (RDCs) are Census Bureau facilities, housed in partner institutions, that meet all physical and information security requirements for access to restricted-use micro data of the agencies whose data are accessed there.

> RDC researchers have access to computing capacity to handle large datasets and complex calculations. Standard statistical, econometric, and programming software, including Stata, SAS®, R, MATLAB and Anaconda python are available in a Linux environment. RDC researchers can collaborate with other RDC researchers across the U.S. through the secure RDC computing environment.

3. Centre d'accès Sécurisé aux Données (CASD)

> CASD is the French remote access facility providing access for research to a vast catalogue of confidential data from Insee (the national statistical institute) and the Statistical Services in the Ministries (SSMs), from the Central Bank as well as from various administrations, government agencies or research institutions. Initially established by Insee (French National Statistical Institute) in 2009, CASD is now a Groupe d’Intérêt Public (Public Interest Group) with a legal status, created by decree of 29 December 2018, bringing together INSEE CNRS (Centre National de la Recherche Scientifique) and several universities, GENES (Groupe des Écoles Nationales d’Économie et Statistique), , École Polytechnique and HEC Paris (Hautes Études Commerciales),. The main purpose of the consortium is to organize and implement secure access services for confidential data for non-profit research, study, evaluation or innovation, mainly public. CASD’s technical architecture is designed to enable the processing of detailed confidential data in a secure environment through remote access, guaranteeing the highest possible level of security while enabling users to benefit from a familiar and complete workspace. 

> All processing is done within the centralized secure IT infrastructure, housed in CASD premises. To access this environment from outside, CASD has designed an autonomous thin client named the “SD-Box” with biometric authentication, simple to install and use with low dependency of the local IT. It may be used by more than one authorized user. Each project has its own server, a secure bubble, within the infrastructure allowing the project members to access, see and process the data. The entire chain of installation and start-up of clusters as well as the maintenance of their software resources is carried out by the CASD-IA team. CASD’s “AI and big data” on-demand services aim to accelerate research, innovation and production in the field of Artificial Intelligence and massive data. CASD acts as a third party between the data holders and the data users once they are authorized by the dedicated authorities and/or the data holders. Outputs are checked according to the rules provided by the data holders. 

> It can also act as a third party for data matching. Its mission is also to promote the technology developed to secure remote access to confidential data (both public and private). CASD currently holds 496 data sources (including many products), thus supporting, without silos, analysis requiring sensitive data from many domains including health data (epidemiological cohorts and medico-administrative data). Remote access is provided across borders with the same conditions to researchers based in universities within the European Union or the United Kingdom and with some specific conditions to North America. The CASD is also a member of IDAN (International Data Access Network), a network of 6 Research Data Centres from 4 countries (France, Germany, Netherlands and the UK which aims at facilitating research use of confidential data between these countries. As a first step, researchers can work remotely on data provided by partners from within their local Research Data Centre, all partners’access points being in all partners premises.

4. Forschungdatenzentren (FDZ)

>The basic objective of the Research Data Centres of the Statistical Offices of the Federation and the Federal States is to not only enable the access to official statistics microdata but to continuously improve it and to adapt it to the changing needs of scientists. FDZ is a generic term and there are a number of FDZ in Germany, including the federal and state systems, and for individual departments within the government. Information about the program presented in the tables throughout the document refers to the RDC program - specifically the federal program, not the FDZ of the federal states, departments, or GESIS. 

> The foundation of the RDC stems from a report of the “Commission on the Improvement of the Informational Infrastructure between Research and Statistics” (KVI). Commissioned by the Federal Ministry of Education and Research (BMBF), it has developed various proposals for improving the interrelations between researchers and statistics in 1999. One of the central recommendations of this commission was to set up Research Data Centres on the premises of the public data producers.

> Based on this recommendation and funded by the Federal Ministry of Education and Research in the start-up phase, the Statistical Offices started the establishment of Research Data Centres. In autumn 2001, the Research Data Centre of the Federal Statistical Office was established. In April 2002, the Research Data Centre of the Statistical Offices of the Federal States followed as joint pilot project.

> Like other countries, Germany has public use files, and microdata to be accessed in a secure facility, but also has scientific use files which can be downloaded by approved users (slightly more detailed than ordinary public-use files), and remote code submission (where user-written code is run against masterfiles by personnel at the RDC). For the purposes of this document we exclude scientific use files from access and use figures as they are not the most useful basis for comparison with CRDCN. The FDZ still requires an application for use of the scientific use file, but it is subsequently downloaded by the researcher and there are no costs associated with vetting output (the file is deemed safe enough). 19% of FDZ activity makes use of scientific use files, this activity is concentrated among master's students.

5. Australian Bureau of Statistics Datalab (ABS)

> DataLab is the ABS’s analysis solution for high-end users who want to undertake real time complex analysis of detailed and integrated microdata. It is a complimentary microdata solution; other Microdata products include TableBuilder and MicrodataDownload. The ABS also have a range of aggregated statistics published on their website.
> DataLab allows researchers to view and analyse unit record information in a safe and secure virtual environment but all analytical output are checked by the ABS before release. DataLab is available to researchers who meet ABS safe people criteria. Researchers must belong to a government, university or public policy organisation. Researchers must belong to an Australian organsiation and access the system only from Australia.

6. DataFirst (DF)

> DataFirst is a research data service dedicated to giving open access to African data. The service promotes high quality research by providing the essential Open Research Data infrastructure for discovering and accessing data and by developing skills among prospective users, particularly in South Africa. DataFirst undertakes research on the quality and usability of national data and encourages data usage and data sharing.

> The data service includes an online research data repository, and a Secure Research Data Centre in which researchers can analyse sensitive restricted access data. These data include person- household - and institution-level variables that could make identification of respondents possible. 

> The datasets in the secure centre come from surveys conducted by and on behalf of government agencies. The most popular data product is the *National Income Dynamics Study (NIDS), a South African household-level panel survey*

7. Israel Social Sciences Data Centre (ISDC)

> Originally in scope as an institution in this analysis because of its similarities to the CRDCN program as an institution that was initiated by university researchers and headquartered at a university in partnership with a central statistical agency and agreements with partner universities. The ISDC does not appear in the tables below as it was shut down in 2019. The institute still had an online presence at the time of the original drafting of the report which described the service as follows:

> Established by the Faculty of Social Sciences at the Hebrew University of Jerusalem, with a mission to collect, preserve and distribute data of interest to the academic community, the ISDC is a major infrastructure for quantitative research of Israeli social and economic issues. Since the early-nineties, it has become a national resource center, disseminating data throughout Israel and, by arrangement with foreign archives, internationally. Its user community includes Israeli universities, colleges and research institutes as well as policy makers in the public and private sectors. The advisory committee, consisting of the member institutions and an observer from the Central Bureau of Statistics, supervises the ISDC activities.
 
> The ISDC now houses approximately 1000 datasets including national sample surveys, local studies, census micro-data and government records in selected fields as well as macro-economic and regional series. Several databases are available through the Web. 

> Sources of data include the Central Bureau of Statistics (CBS), the National Insurance Institute of Israel (NII), central and local government agencies, research institutes as well as independent researchers from affiliated institutes. Since 1995, the ISDC serves also as an authorized distributor of micro-data from Israel Central Bureau of Statistics. Major sources of foreign data are ISSP and ICPSR Archive in Michigan, where ISDC serves as the Official Representative of Israel. 

> The system was one of high-trust where accredited researchers were provided with copies of the microdata through a temporary FTP link or on a CD. Researchers were given guidance on using and publishing statistical information taken from the data and entrusted to use the data for the purposes outlined in their research proposal. The center closed in 2019 due to lack of funding. From the outside, the system did not seem to provide a lot of *added value* as managing the FTP server and performing statistical analyses for hire are functions that could (and seem to have been) easily absorbed by the central statistical agency. The brevity of the communication with the former university lead did not permit inquiry into whether the new arrangement is working more efficiently for both university researchers and the statistical agency, but it is not difficult to think that this represents an overall improvement in access to the *Israeli* data. What academics have most likely lost is institutional access to a local ICPSR member.


## Section 2 - Infrastructure Comparisons

The metrics presented in this section will enable CRDCN to report on its standing relative to international comparator organizations that are positioned within their own national context in the research data ecosystem in a manner similar to CRDCN in Canada. Metrics included in this section will focus primarily on those over which CRDCN has some jurisdiction for decision-making and tracking.

*Table 2.1 - Physical vs. Virtual data access*

|Comparator|CRDCN|FSRDC|CASD|FDZ|ABS|DF|
|---|---|---|---|---|---|---|
|1. Total number of contracts and Researchers|2514 Users, 4972 Projects| 1161 Users, 572 Projects| 1202 Users, 1103 Projects|760 projects | 1772 Users, 503 Projects | 13 Users, 11 Projects|
|2. Share of contracts using a dataset that is available remotely| 0 | 0.13 | 1.0 |1.0* | 1.0 | 0 |
|3. Share of users doing remote work | 0 | 0.10* | 1.0 |0.42| 1.0 | 0 |
|4. Share of new datafiles available virtually| 0 | - | 1.0 |1.0* | 1.0 | 0 |

Notes:

- CRDCN does not currently have a mechanism for remote access. Files above Confidentiality Classification 8 will be accessible only in-person once remote access becomes available. Currently 70% of contracts would be eligible for remote access.

- All new researchers and projects must start their project within the FSRDC facility and may then apply for remote access if sponsoring agency agreements allow. Participation in remote access pilot programs is subject to authorization by the appropriate government agencies, data sponsors, and existing data agreements. Some agencies may approve some of their data for remote access, but not all. Researchers on some projects will not be eligible for remote access because of restrictions associated with a subset of the datasets to which they have access. Census represents the majority of data use in the centres (66%). The data for column three is the share of *projects* doing remote work, not users.

- For CASD, remote access is the only way to access all confidential data via a thin client, the SD-Box, installed in their office in their universities. Remote access is available across borders from the EU countries and associated countries ( + UK) with the same conditions, and with specific conditions from North America

- In the FDZ, remote access is done on the microdata via remote code submission, rather than remote access to the microdata directly (as we generally think of "remote" access, and is implied by the *virtual data access* heading to the table). The German system has a remote access program in development. In contrast to other countries, remote job submission means that researchers from outside Germany are able to access and work on German microdata. 

- ABS does not have physical sites. Remote access is restricted to "approved workspaces" which are generally university offices or home offices - the door must be lockable and cannot be a communal space.

- DataFirst has no virtual access and no mechanism for virtual access to confidential files. They provide an open data repository service for non-confidential data, but data in the centre can identify individuals (contains lat./long. information about collection site) and is more confidential than data contained in similar services in other countries.

*Table 2.2 - Access to data by location/Stakeholders served*

|Comparator|CRDCN|FSRDC|CASD|FDZ|ABS|DF|
|---|---|---|---|---|---|---|
|1. Share of users from academia |0.8 | 0.9 |0.7 |1.0 |0.65 | 1.0| 
|2. Share of users from government |0.05 | 0.1 |0.15 |0.0 | 0.30| 0|
|3. Share of users from private sector|0.18 | <0.01 |0.15 |0.0 | 0.05 |0| 

Notes:

- Totals may differ from 1 due to rounding/users of unknown status. 

- Sometimes FSRDC student researchers graduate from academia to private or public sector positions, but these changes to status are not captured by the shares listed.

- In Germany, anyone not conducting independent academic research through an approved institution (university or think-tank) is ineligible to use the services. Thus 100% of users are academic. These users must still apply for access to specific microdata files once their institution is on the list of approved organizations.

- ABS does not allow undergraduate students to access the system. There is also a status of 'discussant' that exists aside from 'analyst' that allows someone to view the confidential aspects of the project, but not have a login. This reduces workloads on analysts as they don't have to process disclosure requests or review non-numeric outputs (unlabeled graphs etc.) for things that are intermediate results.

- DataFirst serves academics almost exclusively with a few researchers from government using the data. There is no restriction on private sector users working with the data, but the private sector does not have the skills to work with the microdata and so they aren't represented among the users. With all access happening at University of Cape Town, there is no access outside of a major centre.


*Table 2.3 - Data availability*

|Comparator|CRDCN|FSRDC|CASD|FDZ|ABS|DF|
|---|---|---|---|---|---|---|
|1. Files available| 212 | 370 |496| 83|74| 34 | 
|2. Files added in the past year | 38 | - | 80| | 6| 0 |
|3. Updates made to existing files | 31 | 341 | 42| | | 0 |
|4. Number of administrative-survey linked files | 21 | | | | | 0 |

Notes:

- To the maximum extent possible, files are counted consistently across organisations one title = one file even if multiple waves of a survey exist.

- The FSRDC files have update dates in [their system](https://www.researchdatagov.org/search), but dataset updates are not reflected in most of the update date information, thus it isn't possible to determine when the *Data* were updated.

- Most CASD data are multi-year/multi-wave files, not easily disaggregated for consistent counting; 496 is an overstatement of the number of files relative to the other organisations.

- Each [file in the FDZ](https://www.forschungsdatenzentrum.de/de/alle-daten) might have multiple versions: one for scientific use that is less sensitive, and another for secure centre access only. A third in-between version may be created to comply with legislation around remote access once the system is introduced.

- ABS total was taken from [the Datalab Products Page](https://www.abs.gov.au/statistics/microdata-tablebuilder/datalab/products).

- DataFirst data consist of survey files and administrative records. The most recent update to any file was in 2020.


## Section 3 - Governance, Funding and Expenditure Summary

### Governance Model

##### 1. CRDCN:
	
The CRDCN exists as a partnership between over 30 universities in Canada and Statistics Canada. The organization is headquartered at McMaster University with a variety of governing bodies including a Board of Directors, an Academic Council made up of professors representing the institutions which host a data lab, and a small central staff that manages the funds and provide administrative support to the program including some centralized training offerings.

##### 2. FSRDC:

The FSRDC is a program run by the Census Bureau's Center for Enterprise Dissemination through Universities and Federal Reserve banks across the US which play host to RDCs. Funding is provided by the data providers and the RDC sites (who provide cash funding as well as in-kind funding in the form of lab space.) Each site has a lot of control over their own budgeting and funding model which leads to large variations in how projects are charged/who gets charged.

##### 3. CASD:

CASD, as a group whose mission is to implement secure access services, is overseen by representatives of the National statististical Institute/ Insee together with CNRS, Centre national de la recherche scientifique, and several major French universities, GENES (Groupe des écoles nationales d’économie et statistique), HEC Paris and École Polytechnique. There are three committees upon which the director can draw when making decisions: The Scientific Council, the Data Holders Committee, and the Information System Security Policy Monitoring Committee. These groups assist the General Assembly and the Director in matters of strategy, innovation and ethics; access conditions, documentation and archiving; and system security governance respectively. CASD is not in charge of the users’ approval which depend on dedicated independent authorities: for most data approval is based on an advice from the Comité du Secret Statistique, an independent authority chaired by a Judge with representatives of the Parliament, of the Unions, of the researchers and the data holders/data producers, with decision from the National Archives. In any case, the decision requires the approval of the data producer/data holder.
Funding mainly comes from the fees paid by the users (about 65%) and a small part from the consortium partners (25-30%); the remainder varies year-by-year, one example is revenues from the patent on the SD-Box)
9-year funding agreement from EQUIPEX paid a comparable role to CFI in the Canadian context by establishing the infrastructure.

##### 4. FDZ:

The FDZ system is run by the central statistical agency. At least one secure centre is available in each county, with multiple centres available in large cities or counties with several large cities. Costs are partly covered by the government, but user-fees are meant to make up the remainder of the budget. Similar to CASD, each project pays a charge that is specific to their status (student vs. non-student), and data needs. Providing remote job execution is the most costly means of providing access, since the agency vets every piece of output for disclosure, even if the researcher might consider it to be an intermediate output. FDZ is re-evaluating their pricing for modes of access. Remote execution will soon be more costly than secure centre access which will be more costly than scientific use data.

##### 5. ABS:

Datalab is maintained by the Statistical Production and Digital Services (SPDS) Division of the Australian Bureau of Statistics and nis partially funded by the ABS with some government funding and partly funded by users. Universities Australia as a group negotiate a price for access to the Datalab service.

##### 6. DF:

DataFirst is based at the University of Cape Town; the unit’s part-time director is a professor of Economics. DataFirst’s governing bodies include an internal advisory committee made up of Research Deputy Vice-Chancellors, and representatives from commerce and humanities; and a Board of Directors with representatives from ICPSR, UK Data Service, Statistics South Africa, and representatives from the research-intensive universities across the country.


### Expenditures and costs

##### 1. CRDCN:

|Budget item| Description|
|:---|---|
|Cost of a project| A project costs approximately (CAD)6500 for external users (i.e. those not employed by and working for a university in the consortium). There is no recurring cost, but the initial payment covers 200 hours of access. Additional access is purchased in increments of 100 hours at a rate of (CAD)1750.
|Infrastructure support model | The CRDCN is funded by grants from national level government bodies which fund research (SSHRC, CIHR) and research infrastructure (CFI), as well as by provincial governments and university partners. Data are provided by Statistics Canada from their data holdings, they also provide in-kind staff support to CRDCN via the RDC program.|
|Distribution and employment of data custodians | Data custodians are employed by Statistics Canada in the subject matter divisions. In addition to these individuals, the access division which oversees the data in the RDC program have 'persons most responsible' for the data that serve as liaisons between the subject matter division and researchers using the data. |
|Human resources and allocation| CRDCN operates centrally with 13 staff members (An executive director, 3 FTE in research, 3 in communications, 3 in IT, and 2 in Operations/finance). Combining these with the administrative support provided in-kind by Statistics Canada and the partner institutions the total FTE for the network is 85 people. | 

##### 2. FSRDC:

|Budget item| Description|
|:---|---|
|Cost of a project|[Fees are charged](https://www.census.gov/about/adrm/fsrdc/about/fsrdc-network-fees.html) by some data providers to offset the cost of access. Example case is Bureau of Economic Analysis which charges (USD)5,400 for setup and (USD)1,900 annually for the life of the project. Example lab fee is 13,500 (USD) for a five-year project**|
|Infrastructure support model |Federal and state statistical agencies collaborate with the Census Bureau to provide microdata to approved researchers in the secure FSRDC environment. The agencies listed [on this page](https://www.researchdatagov.org/about) contribute data directly to the FSRDCs.|
|Distribution and employment of data custodians | Custodians are employed by the organizations that share their data.|
|Human resources and allocation| Lab directors are employed by the census bureau, each site has its own local director who belongs to the institution hosting the site. There is also a central staff complement at Census Bureau. |

**Project cost example taken from the RDC at Georgetown University (which posts pricing info online). 

##### 3. CASD

|Budget item| Description|
|:---|---|
|Cost of a project| Each project cost is [calculated](https://www.casd.eu/estimation-de-cout/) based on resources and data required (between 2500 and 3500 Euros). All charges are renewed (projects are typically 4 years - 12 months initial contract then 6 month renewals), charges for students are approximately 60% of what others pay. Charges higher for for-profit sector.|
|Infrastructure support model | Software charges are added to project cost (e.g. MS office - €384/yr), 5 system configurations available (cores+ram) - all custom images (i.e. a menu of off-standard software available including more costly and esoteric offerings like Matlab or SPSS) |
|Distribution and employment of data custodians | Custodians are employed by CASD|
|Human resources and allocation| 27 employees (2 provided in kind by INSEE & CNRS), 3 departments (project management service - 7; IT - 12; data management - 6; executive 2) there is more support staff in-kind from partners (but not on a full time basis)|


##### 4. FDZ

|Budget item| Description|
|:---|---|
|Cost of a project| €250/survey-year. The [total charge](https://www.forschungsdatenzentrum.de/en/user-charge) depends on the number of data files requested and number of modes of access. Students pay a reduced rate (€50). Projects last 3 years, but can be extended. |
|Infrastructure support model | The FDZ program is administrated by the statistical agency via a stand-alone division. Project-fees cover a portion of the total cost of servicing a contract.|
|Distribution and employment of data custodians | The agencies that collect the data provide it to the central statistical office who takes over the data custodianship. Once the data are transferred, there isn't any further contact about which projects are being done or any review of researcher proposals. |
|Human resources and allocation| There is a central staff consisting of 9 people, most of whom are former academics, three of whom are contract employees. Each secure centre has a staff-person   |

- Typical project cost is €8000. The average project has 3 researchers on it.

##### 5. ABS:

|Budget item| Description|
|:---|---|
|Cost of a project|(AUD) 2000-2200 annually|
|Infrastructure support model | Created and maintained by ABS, funded by Treasury Board. Project costs for university researchers are paid by Universities Australia as a lump-sum to the ABS as a payment for service. ABS does not charge external agencies to bring data into the system (though any linkages created are charged at cost-recovery). These agencies can also access the data they've provided to the service without the charge. |
|Distribution and employment of data custodians | ABS is responsible for care and security of external data from different departments. Custodians remain in the department sending the data and have to review all requests for researcher access. In some cases, within specific topics, the ABS can approve access to data sets from other departments.|
|Human resources and allocation| Within the ABS, 110 individuals are responsible for the programming (this includes data integration activities and curation.) |

- Costs do not scale down when adding extra researchers (i.e. 10 researchers is double the cost of 5 etc..) 

- ABS is currently reviewing the costs of providing access to data and is anticipating that fees will increase in the near future. In concert with this update to the fees, the agreement with the universities will be revisited.

##### 6. DataFirst:

|Budget item| Description|
|:---|---|
|Cost of a project| There is no charge to access either the online data or confidential data provided in DataFirst's secure centre.|
|Infrastructure support model | Data First is not supported by government funds. The University of Cape Town supports the project from internal funds, but the bulk of revenue comes from offering training in data management and data analysis. DataFirst is able to charge for this training because they are exclusively positioned to offer it. Training is provided not just to agencies in South Africa, but across the continent. |
|Distribution and employment of data custodians | The staff at Data First are able to conduct proposal reviews and grant access. |
|Human resources and allocation | Data First has four staff members: A manager, one full-time and two part-time data analysts.|





